use chrono::{DateTime, TimeZone, Utc};
use clap::{crate_version, App, Arg};
use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

extern crate clap;
use regex::Regex;

fn main() {
    let matches = App::new("timestampcli")
        .version(crate_version!())
        .author("Oscar Hemelaar <ohemelaar@e.email>")
        .about("Converts numeric timestamps to more human-readable formats.")
        .arg(Arg::with_name("filename").value_name("FILE").help(
            "File to show timestamp conversion for. If omitted standard input is used instead.",
        ))
        .arg(
            Arg::with_name("formatter")
                .long("formatter")
                .takes_value(true)
                .possible_values(&["iso", "json-iso"])
                .help("Formatter to use."),
        )
        .get_matches();
    let filename = matches.value_of("filename");
    let to_read: Box<dyn BufRead> = match filename {
        Some(filename) => Box::new(BufReader::new(
            File::open(filename).expect(&format!("File not found: {}", filename)),
        )),
        None => Box::new(BufReader::new(io::stdin())),
    };

    let formatter_name = matches.value_of("formatter").unwrap_or("iso");
    let formatter: fn(DateTime<Utc>) -> String = match formatter_name.as_ref() {
        "iso" => formatter_text_iso,
        "json-iso" => formatter_json_iso,
        _ => formatter_text_iso,
    };
    for line in to_read.lines() {
        println!("{}", timestampify_text(&line.unwrap(), formatter));
    }
}

fn formatter_text_iso(date_time: DateTime<Utc>) -> String {
    date_time.to_rfc3339()
}

fn formatter_json_iso(date_time: DateTime<Utc>) -> String {
    format!("\"{}\"", date_time.to_rfc3339())
}

fn timestampify_text(input: &String, formatter: fn(DateTime<Utc>) -> String) -> String {
    let timestamp_re = Regex::new(r"(^|[^0-9])(?P<t>1[0-9]{9}([0-9]{3}){0,3})([^0-9]|$)").unwrap();
    let captures = timestamp_re.captures_iter(input);
    let mut result = "".to_string();
    let mut prev_end = 0;
    for capture in captures {
        let ts_match = capture.name("t").unwrap();
        let seconds = ts_match.as_str()[0..10].parse::<i64>().unwrap();
        let nanoseconds = parse_nanoseconds(&ts_match.as_str()[10..]);
        let date_time = Utc.timestamp(seconds, nanoseconds);
        result.push_str(&input[prev_end..ts_match.start()]);
        result.push_str(&formatter(date_time));
        prev_end = ts_match.end();
    }
    result.push_str(&input[prev_end..input.len()]);
    result
}

fn parse_nanoseconds(input: &str) -> u32 {
    match input.len() {
        0 => 0,
        3 => input.parse::<u32>().unwrap() * 1_000_000,
        6 => input.parse::<u32>().unwrap() * 1_000,
        9 => input.parse::<u32>().unwrap(),
        _ => todo!("I don't know yet how this error should be handled"),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_text_iso() {
        let input = "lksfdj> 1234567890 <slkfj slfj ' 1234567890 ' sfljk";
        assert_eq!(
            "lksfdj> 2009-02-13T23:31:30+00:00 <slkfj slfj ' 2009-02-13T23:31:30+00:00 ' sfljk",
            timestampify_text(&input.to_string(), formatter_text_iso)
        );
    }

    #[test]
    fn test_json_iso() {
        let input = "lksfdj> 1234567890 <slkfj slfj ' 1234567890 ' sfljk";
        assert_eq!("lksfdj> \"2009-02-13T23:31:30+00:00\" <slkfj slfj ' \"2009-02-13T23:31:30+00:00\" ' sfljk",
		   timestampify_text(&input.to_string(), formatter_json_iso));
    }
}
